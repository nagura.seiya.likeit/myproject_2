<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
<header style="background-color:darkseagreen">
	<div class="container">
        <div class="row">
		<div class="col-md-5 offset-md-3">
			<h1 style="text-align: center">登録データ削除</h1>
		</div>
        <div class="col-md-2 offset-md-2">
        <a href="login.html" style="color: red"> ログアウト </a>
        </div>
        </div>
	</div>
    </header>
    <br>

 <h5 style="text-align: center">このデータを本当に削除してよろしいですか？</h5>
<br>
<div class="container">
  <div class="row">
      <div class="col-2">
    </div>
    <div class="col">
    <button type="button" style="width: 150px;height: 40px"
            onclick="location.href='UserdetailServlet?id=${user.employeeId}'">
            キャンセル
    </button>
    </div>
    <div class="col">
    <form action="UserdeleteServlet" method="post">
        <input type="hidden" name="employeeId" value="${user.employeeId}">
        <input type="submit" style="width: 150px;height: 40px" value="OK">
        <div class="col-2">
    </div>
        </form>
    </div>
  </div>
</div>
    <!--ここまで-->
	<br>
    <footer style="background-color:darkseagreen">
        <p style="text-align: right">© 2020 honyarara</p>
    </footer>
</body>
</html>