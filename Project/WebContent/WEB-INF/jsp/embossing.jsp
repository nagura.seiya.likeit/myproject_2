<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>打刻画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body style="background-color: ghostwhite">
	<header style="background-color: darkseagreen">

		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-3">
					<h1 style="text-align: center">出勤登録</h1>
				</div>
				<div class="col-md-3">
					<a>ログイン中：${userInfo.name}さん</a>
				</div>
				<div class="col-md-2">
					<a href="LogoutServlet" style="color: red"> ログアウト </a>
				</div>
			</div>
		</div>
	</header>
	<!--header end-->
	<!-- errMsg -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<!-- errMsg END -->

	<button onclick="location.href='UserlistServlet'">管理メニュー</button>

	<br>
	<div class="container">
		<div class="row">
			<div class="col-6" style="text-align: center">
				<!-- カレンダー -->
				<div class="bg-dark text-white">
					<p class="h5" id="CalenderArea"></p>
				</div>
				<!-- 現在時刻の表示 -->
				<div class="bg-dark text-white">
					<p class="display-1" id="RealtimeClockArea2"></p>
				</div>
			</div>

			<div class="col-6" style="background-color: white">

				<table class="table table-borderless table-sm">
					<tr>
						<th></th>
						<th>出勤時刻</th>
						<th>休憩入り</th>
						<th>休憩戻り</th>
						<th>退勤時刻</th>
					</tr>

					<c:forEach var="List" items="${attList}">
						<tr>
							<td>${List.userName.name}</td>
							<td>${List.simpleAT}</td>
							<td>${List.simpleBT}</td>
							<td>${List.simpleRT}</td>
							<td>${List.simpleLT}</td>
						</tr>
					</c:forEach>
					</tbody>

				</table>
			</div>

		</div>
		<br>
		<!-- form -->
		<form action="EmbossingServlet" method="post">
			<div class="row">
				<div class="col-2"></div>
				<div class="col">
					<button type="submit" name="push" value="syukkin"
						class="btn btn-outline-info btn-lg">出勤</button>

					<button type="submit" name="push" value="gaisyutu"
						class="btn btn-outline-success btn-lg">外出</button>

					<button type="submit" name="push" value="modori"
						class="btn btn-outline-warning btn-lg">戻り</button>

					<button type="submit" name="push" value="taikin"
						class="btn btn-outline-danger btn-lg">退勤</button>

					<!-- 管理者ならば操作が可能 -->
					<c:if test="${userInfo.loginId.equals('admin')}">
						<button type="submit" name="push" value="execute"
							class="btn btn-danger btn-lg">勤怠確定</button>
					</c:if>
				</div>
			</div>
		</form>
		<!-- form END -->
	</div>


	<br>
	<!--footer-->
	<footer style="background-color: darkseagreen">
		<p style="text-align: right">© 2020 honyarara</p>
	</footer>

	<script type="text/javascript" src="js/calender.js"></script>
	<script type="text/javascript" src="js/clock.js"></script>
</body>
</html>