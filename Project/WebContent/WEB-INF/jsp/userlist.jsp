<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<header style="background-color: darkseagreen">
		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-3">
					<h1 style="text-align: center">従業員一覧</h1>
				</div>
				<div class="col-md-3">
					<a>ログイン中：${userInfo.name}さん</a>
				</div>
				<div class="col-md-2">
					<a href="LogoutServlet" style="color: red"> ログアウト </a>
				</div>
			</div>
		</div>
	</header>
	<button onclick="location.href='EmbossingServlet'">打刻画面へ</button>
	<button onclick="location.href='NewuserServlet'">新規登録</button>
	<br>

	<!-- 検索フォーム -->
	<form action="UserlistServlet" method="post">
		<div class="container">
			<div class="row">
				<div class="col-4"></div>
				<div class="col-2">社員番号</div>
				<div class="col">
					<input type="search" name="inputEmployeeId">
				</div>
			</div>
		</div>

		<br>
		<div class="container">
			<div class="row">
				<div class="col-4"></div>
				<div class="col-2">氏名</div>
				<div class="col">
					<input type="search" name="inputName">
				</div>
			</div>
		</div>

		<br>
		<div class="container">
			<div class="row">
				<div class="col-4"></div>
				<div class="col-2">生年月日</div>
				<div class="col">
					<input type="date" name="inputBirthDateS"> ～ <input
						type="date" name="inputBirthDateE">
				</div>
			</div>
		</div>

		<br>
		<div class="container">
			<div class="row">
				<div class="col-4"></div>
				<div class="col-2">所属</div>
				<div class="col">
					<select name="selectAffiliation">
						<option></option>
						<c:forEach var="aff" items="${affList}">
							<option value="${aff.id}">${aff.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>

		<br>
		<div class="container">
			<div class="row">
				<div class="col-5"></div>
				<div class="col">
					<input type="submit" value="検索" style="width: 140px; height: 35px">
				</div>
				<div class="col-2">
					<a href='UserlistServlet'>検索結果リセット</a>
				</div>
			</div>
		</div>
	</form>

	<br>

	<!-- table -->
	<table class="table table-hover table-sm">
		<tr style="background-color: gainsboro">
			<th>社員番号</th>
			<th>氏名</th>
			<th>生年月日</th>
			<th>所属</th>
			<th>登録日時</th>
			<th></th>
		</tr>

		<c:forEach var="userList" items="${userList}">
			<tr>
				<td>000${userList.employeeId}</td>
				<td>${userList.name}</td>
				<td>${userList.birthDate}</td>
				<td>${userList.affiliationBeans.name}</td>
				<td>${userList.createDate}</td>
				<td><a class="btn-sm btn-info"
					href="UserdetailServlet?id=${userList.employeeId}">詳細</a></td>
			</tr>
		</c:forEach>
	</table>
	<!-- form end -->
	<br>
	<footer style="background-color: darkseagreen">
		<p style="text-align: right">© 2020 honyarara</p>
	</footer>
</body>
</html>