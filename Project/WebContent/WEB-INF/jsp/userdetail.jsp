<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>詳細画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<header style="background-color: darkseagreen">
		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-3">
					<h1 style="text-align: center">詳細画面</h1>
				</div>
				<div class="col-md-3">
					<a>ログイン中：${userInfo.name}さん</a>
				</div>
				<div class="col-md-2">
					<a href="LogoutServlet" style="color: red"> ログアウト </a>
				</div>
			</div>
		</div>
	</header>
	<br>
	<!-- employeeData -->

	<div class="container">
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">社員番号</div>
			<div class="col">000${user.employeeId}</div>
		</div>

		<br>
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">氏名</div>
			<div class="col">${user.name}</div>
		</div>

		<br>
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">生年月日</div>
			<div class="col">${user.birthDate}</div>
		</div>

		<br>
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">所属</div>
			<div class="col">${user.affiliationBeans.name}</div>
		</div>

		<br>
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">登録日時</div>
			<div class="col">${user.createDate}</div>
		</div>

		<br>
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">更新日時</div>
			<div class="col">${user.updateDate}</div>
		</div>

		<br>
		<!-- form -->
		<div class="row">
			<div class="col-md-5 offset-md-5">
				<!-- 管理者ならば更新・削除が可能 -->
				<c:if test="${userInfo.loginId.equals('admin')}">
					<button class="btn btn-success" type="submit"
						onclick="location.href='UserupdateServlet?id=${user.employeeId}'">更新</button>
					<button class="btn btn-danger" type="submit"
						onclick="location.href='UserdeleteServlet?id=${user.employeeId}'">削除</button>
				</c:if>
				<!-- 自分のデータならば更新のみ可能 -->
				<c:if test="${userInfo.loginId == user.loginId}">
					<button class="btn btn-success" type="submit"
						onclick="location.href='UserupdateServlet?id=${user.employeeId}'">更新</button>
				</c:if>
			</div>
		</div>
		<!-- form end -->
	</div>
	<!--employeeData END -->

	<!-- workData -->
	<div class="container">
		<br>
		<div class="col" style="text-align: center">
			<h5>${year}年${month}月度詳細</h5>
		</div>
		<br>

		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">出勤日数</div>
			<div class="col">${tad.totalattendanceDay}日</div>
		</div>

		<br>
		<div class="col row">
			<div class="col-4"></div>
			<div class="col-2">実働時間</div>
			<div class="col">${tad.simpleTAT}</div>
		</div>

		<br>
		<!-- form -->
		<form action="UserdetailServlet?id=${user.employeeId}" method="post">
			<div class="row">
				<div class="col"></div>
				<div class="col"></div>
				<div class="col"></div>
				<div class="col">
					<button class="btn btn-link" type="submit" name="push" value="prev">←前月</button>
				</div>
				<div class="col">
					<button class="btn btn-link" type="submit" name="push" value="back">一覧へ戻る</button>
				</div>
				<div class="col">
					<button class="btn btn-link" type="submit" name="push" value="next">次月→</button>
				</div>
				<div class="col"></div>
				<div class="col"></div>
				<div class="col"></div>
			</div>
		</form>
		<!-- form END -->
	</div>

	<!-- workData END -->
	<br>
	<footer style="background-color: darkseagreen">
		<p style="text-align: right">© 2020 honyarara</p>
	</footer>
</body>
</html>