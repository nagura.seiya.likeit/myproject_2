<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<header style="background-color: darkseagreen">
		<div class="container">
			<div class="row">
				<div class="col-md-5 offset-md-3">
					<h1 style="text-align: center">新規登録</h1>
				</div>
				<div class="col-md-2 offset-md-2">
					<a href="login.html" style="color: red"> ログアウト </a>
				</div>
			</div>
		</div>
	</header>
	<br>

	<!-- エラーメッセージ -->
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<!-- form -->
	<form action="NewuserServlet" method="post">
		<div class="container">
			<br>
			<div class="col row">
				<div class="col-4"></div>
				<div class="col-2">ログインID</div>
				<div class="col">
					<input type="text" name="loginId">
				</div>
			</div>

			<br>
			<div class="col row">
				<div class="col-4"></div>
				<div class="col-2">氏名</div>
				<div class="col">
					<input type="text" name="name">
				</div>
			</div>

			<br>
			<div class="col row">
				<div class="col-4"></div>
				<div class="col-2">パスワード</div>
				<div class="col">
					<input type="password" name="password1">
				</div>
			</div>

			<br>
			<div class="col row">
				<div class="col-4"></div>
				<div class="col-2">パスワード(確認)</div>
				<div class="col">
					<input type="password" name="password2">
				</div>
			</div>

			<br>
			<div class="col row">
				<div class="col-4"></div>
				<div class="col-2">生年月日</div>
				<div class="col">
					<input type="date" value="${user.birthDateStr}" name="birthDate">
				</div>
			</div>

			<br>
			<div class="col row">
				<div class="col-4"></div>
				<div class="col-2">所属</div>
				<div class="col">
					<select name="affiliationId">
						<c:forEach var="aff" items="${affList}">
							<option value="${aff.id}">${aff.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-3"></div>
				<div class="col">
					<button type="button" style="width: 150px; height: 40px"
						onclick="location.href='UserlistServlet'">キャンセル</button>
				</div>
				<div class="col">
					<input type="submit" style="width: 150px; height: 40px" value="登録">
				</div>
			</div>
		</div>
	</form>
	<!-- form end -->
	<br>
	<footer style="background-color: darkseagreen">
		<p style="text-align: right">© 2020 honyarara</p>
	</footer>
</body>
</html>