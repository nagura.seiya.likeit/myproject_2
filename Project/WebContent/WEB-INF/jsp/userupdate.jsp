<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<%@ page import="beans.Affiliation"%>
	<%@ page import="beans.User"%>
	<%
		User user = (User) request.getAttribute("user");
	%>
	<header style="background-color: darkseagreen">
		<div class="container">
			<div class="row">
				<div class="col-md-5 offset-md-3">
					<h1 style="text-align: center">登録情報更新</h1>
				</div>
				<div class="col-md-2 offset-md-2">
					<a href="LogoutServlet" style="color: red"> ログアウト </a>
				</div>
			</div>
		</div>
	</header>
	<br>

	<!-- form -->
	<form action="UserupdateServlet" method="post">

		<input type="hidden" value="${user.employeeId}" name="employeeId">

		<div class="container">
			<c:if test="${userInfo.loginId.equals('admin')}" var="flg" />
			<!-- 管理者による更新の場合 -->
			<c:if test="${flg}">
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">社員番号</div>
					<div class="col">000${user.employeeId}</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">ログインID</div>
					<div class="col">${user.loginId}</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">氏名</div>
					<div class="col">
						<input type="text" name="name" value="${user.name}">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">パスワード</div>
					<div class="col">
						<input type="password" name="password1">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">パスワード(確認)</div>
					<div class="col">
						<input type="password" name="password2">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">生年月日</div>
					<div class="col">
						<input type="date" value="${user.birthDate}" name="birthDate">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">所属</div>
					<div class="col">
						<select name="affiliationId">
							<c:forEach var="aff" items="${affList}">
								<c:if test="${aff.id == user.affiliationId}">
									<option value="${aff.id}" selected>${aff.name}</option>
								</c:if>
								<c:if test="${aff.id != user.affiliationId}">
									<option value="${aff.id}">${aff.name}</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
			</c:if>
			<!-- 一般ユーザーによる更新の場合 -->
			<c:if test="${!flg}">
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">社員番号</div>
					<div class="col">000${user.employeeId}</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">ログインID</div>
					<div class="col">${user.loginId}</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">氏名</div>
					<div class="col">
						<input type="text" name="name" readonly="readonly"
							value="${user.name}">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">パスワード</div>
					<div class="col">
						<input type="password" name="password1">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">パスワード(確認)</div>
					<div class="col">
						<input type="password" name="password2">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">生年月日</div>
					<div class="col">
						<input type="date" readonly="readonly" value="${user.birthDate}"
							name="birthDate">
					</div>
				</div>

				<br>
				<div class="col row">
					<div class="col-4"></div>
					<div class="col-2">所属</div>
					<div class="col">
						<select name="affiliationId">
							<c:forEach var="aff" items="${affList}">
								<c:if test="${aff.id == user.affiliationId}">
									<option value="${aff.id}" selected>${aff.name}</option>
								</c:if>
								<c:if test="${aff.id != user.affiliationId}">
									<option value="${aff.id}" disabled="disabled">${aff.name}</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
			</c:if>

			<br>
			<div class="row">
				<div class="col-3"></div>
				<div class="col">
					<button type="button" style="width: 150px; height: 40px"
						onclick="location.href='UserdetailServlet?id=${user.employeeId}'">キャンセル</button>
				</div>
				<div class="col">
					<input type="submit" style="width: 150px; height: 40px" value="更新">
				</div>
			</div>
		</div>
	</form>

	<!-- form END -->
	<br>
	<footer style="background-color: darkseagreen">
		<p style="text-align: right">© 2020 honyarara</p>
	</footer>
</body>
</html>