package controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Attendance;
import beans.User;
import dao.AttendanceDAO;
import dao.UserDAO;

/**
 * Servlet implementation class EmbossingServlet
 */
@WebServlet("/EmbossingServlet")
public class EmbossingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmbossingServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログインセッションの取得
		HttpSession session = request.getSession();
		User check = (User) session.getAttribute("userInfo");

		//セッションがない場合はログイン画面へ
		if (check == null) {
			request.setAttribute("errMsg", "再度ログインしてください");
			//フォワード(ログイン画面に戻る)
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

		// エラーメッセージがある場合の処理
		if (session.getAttribute("errMsg") != null) {
			request.setAttribute("errMsg", session.getAttribute("errMsg"));
			session.removeAttribute("errMsg");
		}

		//出勤データテーブルの情報を取得してフォワード
		AttendanceDAO attDao = new AttendanceDAO();
		List<Attendance> attList = attDao.findAll();
		request.setAttribute("attList", attList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/embossing.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//セッションからログインIDを取得
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		String loginId = user.getLoginId();
		//ログインIDからユーザー情報を取得
		UserDAO userdao = new UserDAO();
		User userdata = userdao.findUserByLoginId(loginId);

		//社員番号(employeeId)
		int userId = userdata.getEmployeeId();
		//出勤状態(attendanceCheckId)
		int status = userdata.getAttendanceCheckId();

		// リクエストパラメータの入力項目を取得
		String push = request.getParameter("push");

		/**押されたボタンに応じた処理**/

		/**出勤時**/
		if (push.equals("syukkin")) {
			//出勤データの有無をチェック
			AttendanceDAO attdao = new AttendanceDAO();
			Attendance att = attdao.findUserAttendance(userId);
			//データがあった場合は操作不可
			if (att != null) {
				session.setAttribute("errMsg", "本日は既に出勤登録済みです");
				response.sendRedirect("EmbossingServlet");
			} else {

				int attendanceCheckId = 2;

				UserDAO dao = new UserDAO();
				dao.UpdateAttendanceCheckId(attendanceCheckId, loginId);
				AttendanceDAO attDao = new AttendanceDAO();
				attDao.ComingOffice(userId);

				response.sendRedirect("EmbossingServlet");
			}
		}
		/**外出時**/
		if (push.equals("gaisyutu")) {
			//出勤状態でなければ操作不可
			if (status == 1 || status == 3) {
				session.setAttribute("errMsg", "出勤状態ではありません");
				response.sendRedirect("EmbossingServlet");
			} else {
				int attendanceCheckId = 3;

				UserDAO dao = new UserDAO();
				dao.UpdateAttendanceCheckId(attendanceCheckId, loginId);
				AttendanceDAO attDao = new AttendanceDAO();
				attDao.BreakTime(userId);

				response.sendRedirect("EmbossingServlet");
			}
		}
		/**戻り時**/
		if (push.equals("modori")) {
			//休憩状態でなければ操作不可
			if (status != 3) {
				session.setAttribute("errMsg", "外出登録をしていません");
				response.sendRedirect("EmbossingServlet");
			} else {
				int attendanceCheckId = 2;

				//まず戻り時間を登録
				UserDAO dao = new UserDAO();
				dao.UpdateAttendanceCheckId(attendanceCheckId, loginId);
				AttendanceDAO attDao = new AttendanceDAO();
				attDao.ReturnWork(userId);

				//休憩時間を計算して合計する
				attDao.BreakTotal(userId);

				response.sendRedirect("EmbossingServlet");
			}
		}
		/**退勤時**/
		if (push.equals("taikin")) {
			//出勤状態でなければ操作不可
			if (status == 1) {
				session.setAttribute("errMsg", "出勤登録がされていません");
				response.sendRedirect("EmbossingServlet");
			} else if (status == 3) {
				session.setAttribute("errMsg", "外出状態です。戻り登録から行ってください");
				response.sendRedirect("EmbossingServlet");
			} else {
				int attendanceCheckId = 1;

				UserDAO dao = new UserDAO();
				dao.UpdateAttendanceCheckId(attendanceCheckId, loginId);
				AttendanceDAO attDao = new AttendanceDAO();
				attDao.LeavingOffice(userId);

				response.sendRedirect("EmbossingServlet");
			}
		}
		/**勤怠確定の処理**/
		if (push.equals("execute")) {

			//年・月のデータを取得
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;

			AttendanceDAO attDao = new AttendanceDAO();
			attDao.UpdateNullLVT(year, month);

			response.sendRedirect("EmbossingServlet");

		}
	}

}
