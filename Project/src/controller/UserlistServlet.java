package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Affiliation;
import beans.User;
import dao.AffiliationDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserlistServlet
 */
@WebServlet("/UserlistServlet")
public class UserlistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserlistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ユーザー一覧を取得
		UserDAO dao = new UserDAO();
		List<User> userList = dao.findAll();
		request.setAttribute("userList", userList);

		//所属先一覧をDBから取得してフォワード
		try {
			ArrayList<Affiliation> affList = AffiliationDAO.getAllAffiliation();
			request.setAttribute("affList", affList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/**検索処理**/
		//リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String inputEmployeeId = request.getParameter("inputEmployeeId");
		String inputName = request.getParameter("inputName");
		String inputBirthDateS = request.getParameter("inputBirthDateS");
		String inputBirthDateE = request.getParameter("inputBirthDateE");
		String selectAffiliation = request.getParameter("selectAffiliation");

		//検索結果をセット
		UserDAO userDao = new UserDAO();
		List<User> result = userDao.findUser(inputEmployeeId, inputName, inputBirthDateS, inputBirthDateE,
				selectAffiliation);
		request.setAttribute("userList", result);
		//所属先一覧をDBから取得してフォワード
		try {
			ArrayList<Affiliation> affList = AffiliationDAO.getAllAffiliation();
			request.setAttribute("affList", affList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
