package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Affiliation;
import dao.AffiliationDAO;
import dao.UserDAO;

/**
 * Servlet implementation class NewuserServlet
 */
@WebServlet("/NewuserServlet")
public class NewuserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewuserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//所属先一覧をDBから取得してフォワード
		try {
			ArrayList<Affiliation> affList = AffiliationDAO.getAllAffiliation();
			request.setAttribute("affList", affList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String birthDate = request.getParameter("birthDate");
		int affiliationId = Integer.parseInt(request.getParameter("affiliationId"));

		//空欄がある時
		if (loginId == "" || name == "" || password1 == "") {
			request.setAttribute("errMsg", "未入力の項目があります");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワード確認が一致しない時
		if (!password1.equals(password2)) {
			request.setAttribute("errMsg", "パスワードを確認してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDAO dao = new UserDAO();
		dao.InsertUser(loginId, name, password1, birthDate, affiliationId);

		response.sendRedirect("UserlistServlet");
	}

}
