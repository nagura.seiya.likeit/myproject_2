package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Affiliation;
import beans.User;
import dao.AffiliationDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserupdateServlet
 */
@WebServlet("/UserupdateServlet")
public class UserupdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserupdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//URLから社員番号を取得
		String id = request.getParameter("id");

		//idをもとに対象のデータを取得
		UserDAO dao = new UserDAO();
		User user = dao.findUser(Integer.parseInt(id));

		request.setAttribute("user", user);

		//所属先一覧をDBから取得してフォワード
		try {
			ArrayList<Affiliation> affList = AffiliationDAO.getAllAffiliation();
			request.setAttribute("affList", affList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		int employeeId = Integer.parseInt(request.getParameter("employeeId"));
		String name = request.getParameter("name");
		String password1 = request.getParameter("password1");
		String birthDate = request.getParameter("birthDate");
		int affiliationId = Integer.parseInt(request.getParameter("affiliationId"));

		//UPDATE文の実行
		UserDAO dao = new UserDAO();
		dao.UpdateUser(name, password1, birthDate, affiliationId, employeeId);

		//ユーザー詳細にリダイレクト
		response.sendRedirect("UserdetailServlet?id=" + employeeId);
	}

}
