package controller;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Attendance;
import beans.User;
import dao.AttendanceDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserdetailServlet
 */
@WebServlet("/UserdetailServlet")
public class UserdetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserdetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//URLから社員番号を取得
		String id = request.getParameter("id");

		//idをもとに対象のデータを取得
		UserDAO udao = new UserDAO();
		User user = udao.findUser(Integer.parseInt(id));
		request.setAttribute("user", user);

		//年・月のデータを取得
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;

		//月の出勤日数を取得・年月はセッションに保存
		AttendanceDAO adao = new AttendanceDAO();
		Attendance Total = adao.CountAttendance(Integer.parseInt(id), year, month);
		request.setAttribute("tad", Total);

		HttpSession session = request.getSession();
		session.setAttribute("year", year);
		session.setAttribute("month", month);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String push = request.getParameter("push");
		// セッションから年月を取得
		HttpSession session = request.getSession();
		int year = (Integer) session.getAttribute("year");
		int month = (Integer) session.getAttribute("month");

		// 一覧へ戻るが押された場合はセッションから年月を破棄する
		if(push.equals("back")) {
			session.removeAttribute("year");
			session.removeAttribute("month");
			response.sendRedirect("UserlistServlet");
		}

		if (push.equals("prev")) {
			//URLから社員番号を取得
			String id = request.getParameter("id");

			//idをもとに対象のデータを取得
			UserDAO udao = new UserDAO();
			User user = udao.findUser(Integer.parseInt(id));
			request.setAttribute("user", user);

			//月を戻す
			month--;
			if (month < 1) {
				year--;
				month = 12;
			}

			//月の出勤日数を取得
			AttendanceDAO adao = new AttendanceDAO();
			Attendance Total = adao.CountAttendance(Integer.parseInt(id), year, month);
			session.setAttribute("year", year);
			session.setAttribute("month", month);
			request.setAttribute("tad", Total);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
			dispatcher.forward(request, response);
		}
		if (push.equals("next")) {
			//URLから社員番号を取得
			String id = request.getParameter("id");

			//idをもとに対象のデータを取得
			UserDAO udao = new UserDAO();
			User user = udao.findUser(Integer.parseInt(id));
			request.setAttribute("user", user);

			//月を進める
			month++;
			if (month > 12) {
				year++;
				month = 1;
			}

			//月の出勤日数を取得
			AttendanceDAO adao = new AttendanceDAO();
			Attendance Total = adao.CountAttendance(Integer.parseInt(id), year, month);
			session.setAttribute("year", year);
			session.setAttribute("month", month);
			request.setAttribute("tad", Total);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
			dispatcher.forward(request, response);
		}
	}

}
