package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class UserdeleteServlet
 */
@WebServlet("/UserdeleteServlet")
public class UserdeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserdeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//URLから社員番号を取得
		String id = request.getParameter("id");

		//idをもとに対象のデータを取得
		UserDAO dao = new UserDAO();
		User user = dao.findUser(Integer.parseInt(id));

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//DELETE文の実行
		String employeeId = request.getParameter("employeeId");
		UserDAO dao = new UserDAO();
		dao.DeleteUser(employeeId);

		//ユーザー一覧へリダイレクト
		response.sendRedirect("UserlistServlet");
	}

}
