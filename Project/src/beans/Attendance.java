package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;

import dao.UserDAO;

public class Attendance implements Serializable {
	private int id;
	private int userId;
	private Date attendanceDate;
	private Time attendanceTime;
	private Time leavingTime;
	private Time breakTime;
	private Time returnTime;
	private Time totalbreakTime;

	private int totalattendanceDay;
	private Time totalattendanceTime;

	public Attendance() {

	}

	//全データをセット
	public Attendance(int id, int userId, Date attendanceDate, Time attendanceTime, Time leavingTime, Time breakTime,
			Time returnTime,Time totalbreakTime) {
		this.id = id;
		this.userId = userId;
		this.attendanceDate = attendanceDate;
		this.attendanceTime = attendanceTime;
		this.leavingTime = leavingTime;
		this.breakTime = breakTime;
		this.returnTime = returnTime;
		this.totalbreakTime = totalbreakTime;

	}

	//合計の出力用
	public Attendance(int totalattendanceDay, Time totalattendanceTime) {
		this.totalattendanceDay = totalattendanceDay;
		this.totalattendanceTime = totalattendanceTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getAttendanceDate() {
		return attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public Time getAttendanceTime() {
		return attendanceTime;
	}

	public void setAttendanceTime(Time attendanceTime) {
		this.attendanceTime = attendanceTime;
	}

	public Time getLeavingTime() {
		return leavingTime;
	}

	public void setLeavingTime(Time leavingTime) {
		this.leavingTime = leavingTime;
	}

	public Time getBreakTime() {
		return breakTime;
	}

	public void setBreakTime(Time breakTime) {
		this.breakTime = breakTime;
	}

	public Time getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(Time returnTime) {
		this.returnTime = returnTime;
	}

	public Time getTotalbreakTime() {
		return totalbreakTime;
	}

	public void setTotalbreakTime(Time totalbreakTime) {
		this.totalbreakTime = totalbreakTime;
	}

	//出勤データのユーザーIDをもとにユーザー名を取得
	public User getUserName() {
		UserDAO dao = new UserDAO();
		User user = new User();

		user = dao.findUser(this.userId);
		return user;
	}

	//秒数を切り捨てて表示する
	public String getSimpleAT() {
		if (attendanceTime == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(attendanceTime);
	}

	public String getSimpleBT() {
		if (breakTime == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(breakTime);
	}

	public String getSimpleRT() {
		if (returnTime == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(returnTime);
	}

	public String getSimpleLT() {
		if (leavingTime == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(leavingTime);
	}

	public int getTotalattendanceDay() {
		return totalattendanceDay;
	}

	public void setTotalattendanceDay(int totalattendanceDay) {
		this.totalattendanceDay = totalattendanceDay;
	}

	public Time getTotalattendanceTime() {
		return totalattendanceTime;
	}

	public void setTotalattendanceTime(Time totalattendanceTime) {
		this.totalattendanceTime = totalattendanceTime;
	}

	//秒数を切り捨てて表示する
	public String getSimpleTAT() {
		if (totalattendanceTime == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH" + "'h' " + "mm" + "'m'");
		return sdf.format(totalattendanceTime);
	}

}
