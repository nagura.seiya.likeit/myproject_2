package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;

import dao.AffiliationDAO;

public class User implements Serializable {
	private int employeeId;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private int affiliationId;
	private int attendanceCheckId;
	private Timestamp createDate;
	private Timestamp updateDate;

	public User() {

	}

	//ログインセッションの保存
	public User(String loginId, String name) {
		this.setLoginId(loginId);
		this.setName(name);
	}

	//全てのデータをセット
	public User(int employeeId, String loginId, String name, Date birthDate, String password, int affiliationId,
			int attendanceCheckId, Timestamp createDate, Timestamp updateDate) {
		this.employeeId = employeeId;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.affiliationId = affiliationId;
		this.attendanceCheckId = attendanceCheckId;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAffiliationId() {
		return affiliationId;
	}

	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	public int getAttendanceCheckId() {
		return attendanceCheckId;
	}

	public void setAttendanceCheckId(int attendanceCheckId) {
		this.attendanceCheckId = attendanceCheckId;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	// ユーザーの所属IDをもとに所属先名を取得
	public Affiliation getAffiliationBeans() {
		AffiliationDAO dao = new AffiliationDAO();
		Affiliation aff = new Affiliation();
		try {
			aff = dao.getAffiliationById(affiliationId);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return aff;
	}

}
