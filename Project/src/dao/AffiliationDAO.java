package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Affiliation;

public class AffiliationDAO {

	/**DBに登録されている所属先一覧のデータを取得**/
	public static ArrayList<Affiliation> getAllAffiliation() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM affiliation");

			ResultSet rs = st.executeQuery();

			ArrayList<Affiliation> AffiliationList = new ArrayList<Affiliation>();
			while (rs.next()) {
				Affiliation aff = new Affiliation();
				aff.setId(rs.getInt("id"));
				aff.setName(rs.getString("name"));

				AffiliationList.add(aff);
			}

			return AffiliationList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**IDをもとに所属先を取得
	 * @throws SQLException **/
	public static Affiliation getAffiliationById(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM affiliation WHERE id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			Affiliation aff = new Affiliation();
			if (rs.next()) {
				aff.setId(rs.getInt("id"));
				aff.setName(rs.getString("name"));
			}

			return aff;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
