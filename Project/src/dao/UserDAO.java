package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import beans.User;

public class UserDAO {

	/**ログイン情報の検索**/
	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, Encryption(password));

			ResultSet rs = stmt.executeQuery();

			//ログイン失敗時
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");

			return new User(loginidData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**ユーザー一覧の取得**/
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE employee_id != 1";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				int employeeId = rs.getInt("employee_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				int affiliationId = rs.getInt("affiliation_id");
				int attendanceCheckId = rs.getInt("attendance_check_id");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(employeeId, loginId, name, birthDate, password, affiliationId, attendanceCheckId,
						createDate, updateDate);
				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userList;
	}

	/**ユーザー一覧の取得**/
	public List<User> findUser(String inputEmployeeId, String inputName, String inputBirthDateS, String inputBirthDateE,
			String selectAffiliation) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE employee_id != 1";

			if(!inputEmployeeId.equals("")) {
				sql += " AND employee_id = " + inputEmployeeId;
			}

			if(!inputName.equals("")) {
				sql += " AND name LIKE '%" + inputName + "%'";
			}

			if(!inputBirthDateS.equals("")) {
				sql += " AND birth_date >'" + inputBirthDateS + "'";
			}

			if(!inputBirthDateE.equals("")) {
				sql += " AND birth_date <'" + inputBirthDateE + "'";
			}

			if(!selectAffiliation.equals("")) {
				sql += " AND affiliation_id =" + selectAffiliation;
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				int employeeId = rs.getInt("employee_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				int affiliationId = rs.getInt("affiliation_id");
				int attendanceCheckId = rs.getInt("attendance_check_id");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(employeeId, loginId, name, birthDate, password, affiliationId, attendanceCheckId,
						createDate, updateDate);
				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userList;
	}

	/**ユーザー情報の取得(詳細クリック時)**/
	public User findUser(int id) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE employee_id =?";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				int employeeId = rs.getInt("employee_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				int affiliationId = rs.getInt("affiliation_id");
				int attendanceCheckId = rs.getInt("attendance_check_id");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(employeeId, loginId, name, birthDate, password, affiliationId, attendanceCheckId,
						createDate, updateDate);
				return user;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**新規登録(出勤状態は未出勤で登録)**/
	public User InsertUser(String loginId, String name, String password, String birthDate, int affiliationId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id,name,password,birth_date,affiliation_id,attendance_check_id,create_date,update_date) VALUES (?,?,?,?,?,1,now(),now())";

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, Encryption(password));
			stmt.setString(4, birthDate);
			stmt.setInt(5, affiliationId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**ユーザ情報の更新**/
	public User UpdateUser(String name, String password, String birthDate, int affiliationId, int employeeId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name=?, password=?, birth_date=?, affiliation_id=?, update_date=now() WHERE employee_id=?";

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, name);
			stmt.setString(2, Encryption(password));
			stmt.setString(3, birthDate);
			stmt.setInt(4, affiliationId);
			stmt.setInt(5, employeeId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**出勤状態の変更**/
	public User UpdateAttendanceCheckId(int attendanceCheckId, String loginId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET attendance_check_id=?, update_date=now() WHERE login_id=?";

			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, attendanceCheckId);
			stmt.setString(2, loginId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**ログインIDをもとにユーザーデータを取得**/
	public User findUserByLoginId(String loginId) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_id =?";
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, loginId);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				int employeeId = rs.getInt("employee_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				int affiliationId = rs.getInt("affiliation_id");
				int attendanceCheckId = rs.getInt("attendance_check_id");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(employeeId, loginId, name, birthDate, password, affiliationId, attendanceCheckId,
						createDate, updateDate);
				return user;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**ユーザ情報の更新**/
	public User DeleteUser(String employeeId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "DELETE FROM user SET WHERE employee_id=?";

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, employeeId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**パスワード暗号化**/
	public String Encryption(String password) {
		//ハッシュを生成したい元の文字列
		String source = "password";

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}

}
