package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import beans.Attendance;

public class AttendanceDAO {

	/**出勤データテーブルの情報を全て取得**/
	public List<Attendance> findAll() {
		Connection conn = null;
		List<Attendance> attList = new ArrayList<Attendance>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM attendance WHERE attendance_date = CURDATE() ORDER BY leaving_time";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				Date attendanceDate = rs.getDate("attendance_date");
				Time attendanceTime = rs.getTime("attendance_time");
				Time leavingTime = rs.getTime("leaving_time");
				Time breakTime = rs.getTime("break_time");
				Time returnTime = rs.getTime("return_time");
				Time totalbreakTime = rs.getTime("total_breaktime");
				Attendance att = new Attendance(id, userId, attendanceDate, attendanceTime, leavingTime, breakTime,
						returnTime, totalbreakTime);
				attList.add(att);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return attList;
	}

	/**当日の出勤データの有無をチェック**/
	public Attendance findUserAttendance(int userId) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM attendance WHERE user_id =? AND attendance_date = CURDATE()";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				Date attendanceDate = rs.getDate("attendance_date");
				Time attendanceTime = rs.getTime("attendance_time");
				Time leavingTime = rs.getTime("leaving_time");
				Time breakTime = rs.getTime("break_time");
				Time returnTime = rs.getTime("return_time");
				Time totalbreakTime = rs.getTime("total_breaktime");
				Attendance att = new Attendance(id, userId, attendanceDate, attendanceTime, leavingTime, breakTime,
						returnTime, totalbreakTime);
				return att;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**月の合計の取得**/
	public Attendance CountAttendance(int userId, int year, int month) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT COUNT(user_id) AS Attendanced_Day,sec_to_time(SUM(time_to_sec(leaving_time))-SUM(time_to_sec(attendance_time))-SUM(time_to_sec(total_breaktime))) AS Attendanced_Time FROM attendance WHERE user_id = ? AND attendance_date LIKE ?";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);
			stmt.setString(2, year + "-0" + month + "%");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int count = rs.getInt("Attendanced_Day");
				Time attTime = rs.getTime("Attendanced_Time");
				Attendance att = new Attendance(count, attTime);
				return att;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**出勤時**/
	public Attendance ComingOffice(int userId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO attendance(user_id,attendance_date,attendance_time) VALUES (?,CURDATE(),now())";

			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**退勤時**/
	public Attendance LeavingOffice(int userId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE attendance SET leaving_time = now() WHERE user_id = ? AND attendance_date = CURDATE()";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**外出時**/
	public Attendance BreakTime(int userId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE attendance SET break_time = now() WHERE user_id = ? AND attendance_date = CURDATE()";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**戻り時**/
	//戻り時間の登録
	public Attendance ReturnWork(int userId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE attendance SET return_time = now()  WHERE user_id = ? AND attendance_date = CURDATE()";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	//休憩時間の合算
	public Attendance BreakTotal(int userId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE attendance SET total_breaktime = sec_to_time(time_to_sec(total_breaktime) + (time_to_sec(return_time) - time_to_sec(break_time))) WHERE user_id = ? AND attendance_date = CURDATE()";
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, userId);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	//退勤時間の強制確定
	public Attendance UpdateNullLVT(int year, int month) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE attendance SET leaving_time = '19:00:00' WHERE leaving_time IS NULL AND attendance_date LIKE ?";
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, year + "-0" + month + "%");

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
