--
--�o�Ύ�--
--
INSERT INTO attendance(user_id,attendance_date,attendance_time)
VALUES (?,CURDATE(),now());

--
--�O�o��--
--
UPDATE attendance SET break_time = now()
WHERE user_id = ?
AND attendance_date = CURDATE();

--
--�߂莞--
--
UPDATE attendance SET return_time = now()
WHERE user_id = ?
AND attendance_date = CURDATE();

--
--�ދΎ�--
--
UPDATE attendance SET leaving_time = now()
WHERE user_id = ?
AND attendance_date = CURDATE();
