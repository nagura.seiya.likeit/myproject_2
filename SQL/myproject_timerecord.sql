CREATE DATABASE myproject_timerecord DEFAULT CHARACTER SET utf8;

USE myproject_timerecord;

--
--ユーザーテーブル--
--
CREATE TABLE user(
employee_id SERIAL,
login_id varchar(100) UNIQUE NOT NULL,
name varchar(20) NOT NULL,
birth_date date NOT NULL,
password varchar(100) NOT NULL,
affiliation_id int NOT NULL,
attendance_check_id int NOT NULL,
create_date DATETIME default now(),
update_date DATETIME default now()
);

--
--所属テーブル--
--
CREATE TABLE affiliation(
id SERIAL,
name varchar(100) NOT NULL
);

--
--勤怠状況テーブル--
--
CREATE TABLE attendance_check(
id SERIAL,
name varchar(20) NOT NULL
);

--
--出勤テーブル--
--
CREATE TABLE attendance(
id SERIAL,
user_id int NOT NULL,
attendance_date DATE NOT NULL,
attendance_time TIME,
leaving_time TIME,
break_time TIME,
return_time TIME,
total_breaktime TIME DEFAULT 0;
);
